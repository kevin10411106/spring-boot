package com.tgl.demo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.tgl.demo.employee.Employee;

//@Mapper
public interface EmployeeMapper {
	Employee findById(int id);
	int insert(Employee emp);
	int deleteById(int id);
	int update(Employee emp);
	void batchinsert(@Param("Employeelist")List<Employee> emp);
}
