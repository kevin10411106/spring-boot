package com.tgl.demo.employee;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import com.opencsv.bean.CsvBindByPosition;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Component
public class Employee {
	
	@CsvBindByPosition(position = 0)
	@Min(1)
	@NotNull
	private int ID;
	
	@CsvBindByPosition(position = 1)
	@NotNull
	private float height;
	
	@CsvBindByPosition(position = 2)
	@NotNull
	private float weight;
	
	@CsvBindByPosition(position = 3)
	@Pattern(regexp = "^[a-zA-Z]{1,16}-[a-zA-Z]{1,16}$",message="Your EnlishName specification is wrong")
	@NotNull
	private String englishName;
	
	@CsvBindByPosition(position = 4)
	@Size(min = 2, max = 4)
	@NotNull
	private String chineseName;
	
	@CsvBindByPosition(position = 5)
	@Size(min = 4, max = 4,message="Please enter four digits")
	@NotNull
	private String ext;
	
	@Email
	@CsvBindByPosition(position = 6)
	@NotNull
	private String email;
	private float BMI;
	
	public Employee() {
		
	}
	public Employee(int ID,float Height, float Weight, String englishName, String chineseName, String Ext, String Email, float BMI) {
		this.height = Height;
		this.weight = Weight;
		this.englishName = englishName;
		this.chineseName = chineseName;
		this.ext = Ext;
		this.email = Email;
		this.BMI = BMI;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}
	
	public float BMI(float H , float W) {
		float bmi = weight/((height/100)*(height/100));
		return bmi;
	}
	
	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public float getBMI() {
		float BMI = weight/((height/100)*(height/100));
		return BMI;
	}

	public void setBMI(float bMI) {
		BMI = bMI;
	}
	@Override
	public String toString() {
		return "Employee [ID = " + ID + ", H = " + height + ", W = " + weight + ", EN = " + englishName + 
				", CN = " + chineseName + ", Ext = " + ext + ", Email = " + email + ", BMI = " + BMI + "]";
	}
	
}
