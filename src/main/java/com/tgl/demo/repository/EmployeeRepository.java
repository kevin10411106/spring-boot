package com.tgl.demo.repository;

import java.awt.geom.RoundRectangle2D.Float;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tgl.demo.employee.Employee;

@Repository
public class EmployeeRepository {
	private static final Logger log = LogManager.getLogger(EmployeeRepository.class);
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public Employee findById(int id) {
		String sql = "select * from stafftable where id = ?";
		RowMapper<Employee> rowMapper = new BeanPropertyRowMapper<Employee>(Employee.class);
		Employee emp = jdbcTemplate.queryForObject(sql, rowMapper,id);
		return emp;
	}
	
	public int insert(Employee emp) {
		String sql = "INSERT INTO stafftable(ID,Height,Weight,EnglishName,ChineseName,Ext,Email,BMI)"
        		+ " VALUES(:id,:height,:weight,:englishName,:chineseName,:ext,:email,:bmi)";
		MapSqlParameterSource msps = new MapSqlParameterSource();
		float bmi = emp.getWeight()/((emp.getHeight()/100)*(emp.getHeight()/100));
		msps.addValue("id", emp.getID());
		msps.addValue("height", emp.getHeight());
		msps.addValue("weight", emp.getWeight());
		msps.addValue("englishName", emp.getEnglishName());
		msps.addValue("chineseName", emp.getChineseName());
		msps.addValue("ext", emp.getExt());
		msps.addValue("email", emp.getEmail());
		msps.addValue("bmi", bmi);
		return namedParameterJdbcTemplate.update(sql, msps);
	}
	
	public int update(Employee emp) {
		String sql = "update stafftable set ID = :id, Height = :height, Weight = :weight, EnglishName = :englishName,"
				+ " ChineseName = :chineseName, Ext = :ext, Email = email, BMI = :bmi where id = :id";
		MapSqlParameterSource msps = new MapSqlParameterSource();
		msps.addValue("id", emp.getID());
		msps.addValue("height", emp.getHeight());
		msps.addValue("weight", emp.getWeight());
		msps.addValue("englishName", emp.getEnglishName());
		msps.addValue("chineseName", emp.getChineseName());
		msps.addValue("ext", emp.getExt());
		msps.addValue("email", emp.getEmail());
		msps.addValue("bmi", emp.getBMI());
		return namedParameterJdbcTemplate.update(sql, msps);
	}
	
	public int deleteById(int id) {
		String sql = "delete from stafftable where id = :id";
		MapSqlParameterSource msps = new MapSqlParameterSource("id",id);
		return namedParameterJdbcTemplate.update(sql, msps);
	}
	
	@Transactional
	public void batchinsert(List<Employee> emp){
		jdbcTemplate.batchUpdate("INSERT INTO stafftable(ID,Height,Weight,EnglishName,ChineseName,Ext,Email,BMI) VALUES(?,?,?,?,?,?,?,?)",new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setInt(1, emp.get(i).getID());
				ps.setFloat(2, emp.get(i).getHeight());
				ps.setFloat(3,emp.get(i).getWeight());
				ps.setString(4,emp.get(i).getEnglishName());
				ps.setString(5, emp.get(i).getChineseName());
				ps.setString(6, emp.get(i).getExt());
				ps.setString(7, emp.get(i).getEmail());
				ps.setFloat(8, emp.get(i).getBMI());
			}
			
			public int getBatchSize() {
				return emp.size();
			}
		});
	}
	
	public List<Employee> findAll(){
		String sql = "select * from stafftable";
		RowMapper<Employee> rowMapper = new BeanPropertyRowMapper<Employee>(Employee.class);
		List<Employee> employee = jdbcTemplate.query(sql, rowMapper);
		log.info("employee size:{}",employee.size());
		return employee;
	}
	
	public boolean checkId(int id) {
		String sql = "select count(*) from stafftable where id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] {id}, Integer.class) > 0;
	}
	
	public boolean checkExt(String ext) {
		String sql = "select count(*) from stafftable where ext = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] {ext}, Integer.class) > 0;
	}
	
	public boolean checkEmail(String email) {
		String sql = "select count(*) from stafftable where email = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] {email}, Integer.class) > 0;
	}
//	public List<Employee> downloadFile(){
//		String sql = "select * from stafftable";
//		RowMapper<Employee> rowMapper = new BeanPropertyRowMapper<Employee>(Employee.class);
//		List<Employee> emp = jdbcTemplate.query(sql, rowMapper);
//		log.info("employee size:{}",emp.size());
//		return emp;
//	}
}
