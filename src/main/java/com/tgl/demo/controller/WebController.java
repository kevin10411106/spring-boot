package com.tgl.demo.controller;


import javax.validation.Valid;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.tgl.demo.employee.Employee;
import com.tgl.demo.server.EmployeeService;

@Controller
@RequestMapping("/web")
public class WebController {
	//private static final Logger log = LogManager.getLogger(WebController.class);
	@Autowired
	private EmployeeService service;
	
	@GetMapping(value = "/list")
	public String findById(Model model) {
		model.addAttribute("employee", service.findAll());
		return "index";
	}
	
	@GetMapping("/post")
	public String insert(Employee emp) {
		return "add";
	}
	
	@PostMapping("/add")
	public String addEmployee(@Valid Employee emp, BindingResult result, Model model) {
		if(result.hasErrors()) {
			return "add";
		}
		service.insert(emp);
		return "redirect:/web/list";
	}
	
	@GetMapping("/edit/{id}")
	public String showUpdateForm(@PathVariable("id") int id, Model model) {
		Employee emp = service.findById(id);
		model.addAttribute("employee", emp);
		return "edit";
	}
	
	@PostMapping("/update")
    public String updateEmployee(@Valid Employee emp, BindingResult result, Model model) {
		if(result.hasErrors()) {
			return "edit";
		}
        service.update(emp);
        model.addAttribute("employee", service.findAll());
        return "redirect:/web/list";
    }
	
	@GetMapping("/delete/{id}")
    public String deleteStudent(@PathVariable("id") int id, Model model) {
        service.deleteById(id);
        model.addAttribute("employee", service.findAll());
        return "index";
    }
	
	@GetMapping("/findById")
	public String showEmployee(@RequestParam (value = "search", required = false) int id, Model model) {
		model.addAttribute("search", service.findById(id));
		return "index";
	}
	
	@GetMapping("/login")
	public String login() {
		return "login";
	}
}
