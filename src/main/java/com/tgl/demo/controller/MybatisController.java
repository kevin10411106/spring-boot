package com.tgl.demo.controller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tgl.demo.employee.Employee;
import com.tgl.demo.server.MybatisService;

@RestController
@RequestMapping("/mybatis")
public class MybatisController {
	@Autowired
	MybatisService mybatisservice;
	
	@GetMapping("/findById")
	public Employee findById(@RequestParam int id) {
		return mybatisservice.findById(id);
	}
	
	@PostMapping("/insert")
	public int insert(@Valid@RequestBody Employee emp) {
		return mybatisservice.insert(emp);
	}
	
	@PutMapping("/update")
	public int update(@Valid@RequestBody Employee emp) {
		return mybatisservice.update(emp);
	}
	
	@DeleteMapping("/deleteById")
	public int deleteById(@RequestParam int id) {
		return mybatisservice.deleteById(id);
	}
	
	@PostMapping("/upload")
	public void singleFileUpload(@RequestParam("file") MultipartFile file,RedirectAttributes redirectAttributes) 
	{
		List<Employee> Employeelist = new ArrayList<Employee>();
        if (!file.isEmpty()) {
            try {
            	String line = null;
                InputStream fi = new BufferedInputStream(file.getInputStream());
                BufferedReader read = new BufferedReader(new InputStreamReader(fi,StandardCharsets.UTF_8));
                while((line = read.readLine())!= null) {
    				String[] token = line.split(",");
    				Employee temp = new Employee(Integer.parseInt(token[0]),Float.parseFloat(token[1]),Float.parseFloat(token[2]), token[3], 
    						token[4],token[5],token[6],Float.parseFloat(token[7]));
    				Employeelist.add(temp);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        mybatisservice.batchinsert(Employeelist);
    }
}
