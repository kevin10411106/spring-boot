package com.tgl.demo.controller;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.tgl.demo.employee.Employee;
import com.tgl.demo.server.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	private static final Logger log = LogManager.getLogger(EmployeeController.class);
	@Autowired
	private EmployeeService service;
	
	@GetMapping(value = "/findById")
	public Employee findById(@RequestParam int id) {
		return service.findById(id);
	}
	
	@PostMapping("/insert")
	public int insert(@Valid@RequestBody Employee emp) {
		return service.insert(emp);
	}
	
	@PutMapping("/update")
	public int update(@Valid@RequestBody Employee emp) {
		return service.update(emp);
	}
	
	@DeleteMapping(value = "/delete")
	public int deleteById(@RequestParam int id) {
		return service.deleteById(id);
	}
	
	@PostMapping("/upload")
    public void singleFileUpload(@RequestParam("file") MultipartFile file,RedirectAttributes redirectAttributes) 
	{
		List<Employee> Employeelist = new ArrayList<Employee>();
        if (!file.isEmpty()) {
            log.info("success!!");
            try {
            	String line = null;
                InputStream fi = new BufferedInputStream(file.getInputStream());
                BufferedReader read = new BufferedReader(new InputStreamReader(fi,StandardCharsets.UTF_8));
                while((line = read.readLine())!= null) {
    				String[] token = line.split(",");
    				Employee temp = new Employee(Integer.parseInt(token[0]),Float.parseFloat(token[1]),Float.parseFloat(token[2]), token[3], 
    						token[4],token[5],token[6],Float.parseFloat(token[7]));
    				Employeelist.add(temp);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        service.batchinsert(Employeelist);
    }
	
	@GetMapping("/checkId")
	public boolean checkId(@RequestParam int id) {
		return service.checkId(id);
	}
	
	@GetMapping("/checkExt")
	public boolean checkExt(@RequestParam String ext) {
		return service.checkExt(ext);
	}
	
	@GetMapping("/checkEmail")
	public boolean checkEmail(@RequestParam String email) {
		return service.checkEmail(email);
	}
//	@GetMapping("/download")
//	public void downloadFile(HttpServletResponse response) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException{
//		//set file name and content type
//        String filename = "employee.csv";
//        response.setContentType("text/csv");
//        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
//                "attachment; filename=\"" + filename + "\"");
//
//        //create a csv writer
//        StatefulBeanToCsv<Employee> writer = null;
//        writer = new StatefulBeanToCsvBuilder<Employee>(response.getWriter())
//                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
//                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
//                .withOrderedResults(false)
//                .build();
//        //write all users to csv file
//        writer.write(service.downloadFile());
//	} 
	
}
