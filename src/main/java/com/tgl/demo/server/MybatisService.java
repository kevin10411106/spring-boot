package com.tgl.demo.server;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgl.demo.employee.Employee;
import com.tgl.demo.mapper.EmployeeMapper;

@Service
public class MybatisService {
	@Autowired
	EmployeeMapper empmapper;
	
	public Employee findById(int id) {
		return empmapper.findById(id);
	}
	
	public int insert(Employee emp) {
		return empmapper.insert(emp);
	}
	
	public int update(Employee emp) {
		return empmapper.update(emp);
	}
	
	public int deleteById(int id) {
		return empmapper.deleteById(id);
	}
	public void batchinsert(List<Employee> emp) {
		empmapper.batchinsert(emp);
}
	
}
