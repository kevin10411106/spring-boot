package com.tgl.demo.server;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgl.demo.aspect.LogExecutionTime;
import com.tgl.demo.employee.Employee;
import com.tgl.demo.repository.EmployeeRepository;


@Service
public class EmployeeService {
	@Autowired
	EmployeeRepository empr;
	
	@LogExecutionTime
	public Employee findById(int id) {
		return empr.findById(id);
	}
	
	public int insert(Employee emp) {
		return empr.insert(emp);
	}
	
	public int update(Employee emp) {
		return empr.update(emp);
	}
	
	public int deleteById(int id) {
		return empr.deleteById(id);
	}
	
	public void batchinsert(List<Employee> emp) {
			empr.batchinsert(emp);
	}
	
	public List<Employee> findAll() {
		return empr.findAll();
	}
	
	public boolean checkId(int id) {
		return empr.checkId(id);
	}
	
	public boolean checkExt(String ext) {
		return empr.checkExt(ext);
	}
	
	public boolean checkEmail(String email) {
		return empr.checkEmail(email);
	}
//	public List<Employee> downloadFile() {
//		return empr.downloadFile();
//	}
}
