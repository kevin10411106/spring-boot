package com.tgl.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	 	@Override
	    protected void configure(HttpSecurity http) throws Exception
	    {
	        http
	         .csrf().disable().authorizeRequests().antMatchers("/","web","/","list").permitAll().anyRequest().authenticated()
	         .and()
	         .formLogin()
	         .loginPage("/web/login")
	         .loginProcessingUrl("/login").usernameParameter("username").passwordParameter("password")
	         .defaultSuccessUrl("/web/list").permitAll()
	         .and()
	         .logout()
	         .logoutUrl("/logout").logoutSuccessUrl("/web/login")
	         .and()
	         .rememberMe();
	    }
	    @Autowired
	    public void configureGlobal(AuthenticationManagerBuilder auth)throws Exception
	    {
	        auth.inMemoryAuthentication()
	            .withUser("kevin").password("{noop}10411106").roles("USER")
	            .and()
                .withUser("admin").password("{noop}password").roles("ADMIN");
	    }
}
